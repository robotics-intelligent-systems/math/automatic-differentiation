#!/bin/bash

# generate version file before code compilation
rm -f containmentFoamVersion.H

export wmcompile='#define __containmentFoam_compile__  '
export gccversion='#define __containmentFoam_gcc_version__  '
export mpiversion='#define __containmentFoam_mpi_version__  '

export repo='#define __containmentFoam_repository__  '
export branch='#define __containmentFoam_branch__  '
export commit='#define __containmentFoam_commit_hash__ '
export date='#define  __containmentFoam_compile_date__ '

# --repo
export repo1=`basename -s .git \`git config --get remote.origin.url\``

# --branch
export branch1=`git status | head -1 | awk  '{ print $3 }'`
if [ -v CI ]
then
   export branch1=$CI_COMMIT_BRANCH
fi

# --revision
export revision1=`git log -n 1 | head -n 1 | sed -e 's/^commit //' | head -c 8`


export wmcompile1=`echo $WM_COMPILE_OPTION`
export gccversion1=`gcc --version | head -n 1`
export mpiversion1=`mpirun --version | head -n 1`



# --date
export date1=`date`

echo  $wmcompile'" compiler option  : '$wmcompile1'"' >  containmentFoamVersion.H
echo  $gccversion'" gcc version      : '$gccversion1'"' >>  containmentFoamVersion.H
echo  $mpiversion'" mpi version      : '$mpiversion1'"' >>  containmentFoamVersion.H

echo  $repo'" repository       : '$repo1'"' >>  containmentFoamVersion.H
echo  $branch'" branch           : '$branch1'"' >> containmentFoamVersion.H
echo  $commit'" commit revision  : '$revision1'"' >> containmentFoamVersion.H
echo  $date'" compiled on      : '$date1'"' >> containmentFoamVersion.H



echo  "" >> containmentFoamVersion.H
echo  "{
Info<<endl<<endl<<endl;
Info<<\"// * * * * * * * * * * * * containmentFOAM version info  * * * * * * * * * * //\"<<endl;
Info<<__containmentFoam_compile__<<endl
<<__containmentFoam_gcc_version__<<endl
<<__containmentFoam_mpi_version__<<endl
<<__containmentFoam_repository__<<endl
<<__containmentFoam_branch__<<endl
<<__containmentFoam_commit_hash__<<endl
<<__containmentFoam_compile_date__<<endl;
Info<<\"// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //\"<<endl;
Info<<endl<<endl<<endl;
}" >> containmentFoamVersion.H

# cd code compilation
CFPATH=$(cd `dirname $0` && pwd)

echo "COMPILING turbulenceModels LIBRARY"
cd $CFPATH/containmentFoamLibs/momentumTransportModels/ || exit &&
wclean all &&
wmake -j $PIPE_NPROC || exit &&

echo "COMPILING massDiffusionModels LIBRARY"
cd $CFPATH/containmentFoamLibs/multiSpeciesMassTransportModels/ || exit &&
wclean all &&
wmake -j $PIPE_NPROC || exit &&

echo "COMPILING boundaryConditions LIBRARY"
cd $CFPATH/containmentFoamLibs/boundaryConditions/ || exit &&
wclean all &&
wmake -j $PIPE_NPROC || exit &&

echo "COMPILING adaptiveTimeStepping LIBRARY"
cd $CFPATH/containmentFoamLibs/adaptiveTimeStepping/ || exit &&
wclean all &&
wmake -j $PIPE_NPROC || exit &&

echo "COMPILING functionObjects LIBRARY"
cd $CFPATH/containmentFoamLibs/functionObjects/ || exit &&
wclean all &&
wmake -j $PIPE_NPROC || exit &&

echo "COMPILING radiationModels LIBRARY"
cd $CFPATH/containmentFoamLibs/radiationModels/ || exit &&
wclean all &&
wmake -j $PIPE_NPROC || exit &&

echo "COMPILING systemModels LIBRARY"
cd $CFPATH/containmentFoamLibs/systemModels/PARs/ || exit &&
sh makeRD.sh || exit &&
cd $CFPATH/containmentFoamLibs/systemModels/ || exit &&
wclean all &&
wmake -j $PIPE_NPROC || exit &&

echo "COMPILING condensation LIBRARY"
cd $CFPATH/containmentFoamLibs/condensationModels/ || exit &&
wclean all &&
wmake -j $PIPE_NPROC || exit &&


echo "COMPILING SOLVERS"

#echo "COMPILING containmentFluidFoam_OF9"
#cd $CFPATH/containmentFluidFoam_OF9/ || exit &&
#wclean all &&
#wmake -j $PIPE_NPROC || exit &&

echo "COMPILING containmentFluidFoam"
cd $CFPATH/containmentFluidFoam/ || exit &&
wclean all &&
wmake -j $PIPE_NPROC || exit &&

echo "COMPILING containmentFoam"
cd $CFPATH/containmentFoam/ || exit &&
wclean all &&
wmake -j $PIPE_NPROC || exit &&
cd $CFPATH

echo "COMPILATION COMPLETE"

Documentation
=============

![](doc/files/images/logo_containmentFOAM.png)

_containmentFOAM_ is a multi-species and multi-physics toolbox based on OpenFOAM-9. It is developed on the background of efficiently simulating transport processes inside confined domains e.g., a nuclear reactor containment in the course of a severe accident. In perspective it may be employed in technical scale safety assessment. It contains submodels to account for pressurization, heat and mass transfer in buoyant flows, gas radiation heat transport, combustible gas (H<sub>2</sub>/CO)-mixing and mitigation as well as aerosol particle transport.
 
# Installation

* _containmentFOAM_ models and libraries can be compiled individually using `wmake`. To ensure all libraries are built and linked in the correct order, it is recommended to use the shell script `Allwmake.sh`

* Dependencies: 
  - [OpenFOAM version 9](https://github.com/OpenFOAM/OpenFOAM-9)
  - Python (Version 3.x) is used for postProcessinng
  - gfortran is used to build REKODIREKT

* A Java based GUI for containmentFoam (cfGUI) is developed to assist the _containmentFoam_ users in quickly setting-up reproducible and consistent analysis cases for containmentFoam. Besides, the cfSolutionMonitor is developed to enable a clear runtime monitoring of simulation runs. It uses a flexible regular expression sytax to evaluate different types of text logs and visualize them as monitors or logs. It can be downloaded in its [repository](https://iffgit.fz-juelich.de/containmentfoam_developers/cfframework_release)
<img src="doc/files/images/cfframework.png" width="95%" height="95%">. 

 

# Documentation               

* [Doxygen documentation](https://containmentfoam_developers.iffgit.fz-juelich.de/containmentfoam_release9/index.html)
* [Basic functionalities and models](doc/files/THEORY.md)
* [Pre- & Postprocessing tools](doc/files/TOOLS.md)
* [New & upcoming features](doc/files/WIP.md)
* [Verification and Validation](doc/files/V_V.md)
* [Debugging](doc/files/DEBUGGING.md)
* [Contributors](CONTRIBUTING.md)
* [References & Publications](doc/files/REFERENCES.md)
* [Change log](CHANGELOG.md)

# How to contribute
* report issues, bugs and wishes as [git issue](https://iffgit.fz-juelich.de/containmentfoam_developers/containmentfoam_release9/-/issues) or in the [forum](https://foam-for-nuclear.org/phpBB/viewforum.php?f=22&sid=9c79ebdfab06e0107108f480680f5f1c) 
* share validation experience and cases.
* contact us to obtain developer status [:envelope:](mailto:containmentfoam@fz-juelich.de)
* to provide code, use git as described [here](doc/files/GIT.md)

# Acknowledgements
_containmentFOAM_ is developed within the Helmholtz [NUSAFE](https://www.fz-juelich.de/iek/iek-6/EN/Research/NUSAFE/nusafe_node.html) program. Substantial contributions are made in the frame of PhD and student research projects, funded by the [RWTH-IITM strategic partnership programm](https://www.rwth-aachen.de/go/id/lwzg/lidx/1), the [DAAD UGC](https://www.daad.de/en/study-and-research-in-germany/), the [CSC](https://www2.daad.de/deutschland/stipendium/datenbank/en/21148-scholarship-database/?detail=50015315), the [HITEC Graduate School](https://www.hitec-graduate-school.de/) and the Federal Ministry for the Environment, Nature Conservation, Nuclear Safety and Consumer Protection (BMUV) (SETCOM-2 Project No. 1501591 in collaboration with RWTH Aachen University and UQ4CFD Project No. 1501595 in collaboration with the UniBW Munich). Code maintenance is supported by BMUV within the CF2REF project (No. 1501633B) in collaboration with GRS gGmbH.

We gratefully acknowledge our collaboration with Prof. K. Arul Prakash (Indian Institute of Technology, Madras). His continious support has substantially aided the developement of _containmentFOAM_.

Finally, we acknowledge the substantial work and efforts taken by the OpenFOAM Foundation, ESI and the the OpenFOAM community for maintaing OpenFOAM.

# Open Reference
Kelm S, Kampili M, Liu X, George A, Schumacher D, Druska C, Struth S, Kuhr A, Ramacher L, Allelein H-J, Prakash KA, Kumar GV, Cammiade LMF, Ji R. 'The Tailored CFD Package ‘containmentFOAM’ for Analysis of Containment Atmosphere Mixing, H2/CO Mitigation and Aerosol Transport'. _Fluids_. 2021; 6(3):100. https://doi.org/10.3390/fluids6030100 


# Disclaimer
This code is developed according to the developers knowledge and experience in OpenFOAM. The users should be aware that there is a chance of bugs in the code, though we've thoroughly test it. The source code may not be fully in the OpenFOAM coding style, and it might not be making use of inheritance of classes to full extent.

This offering is not approved or endorsed by the OpenFOAM Foundation nor OpenCFD Limited, producer and distributor of the OpenFOAM(R)software via
 www.openfoam.com, and owner of the OPENFOAM(R) and OpenCFD(R) trademarks.


<!---
relevant formatting options:

# This indicates a major headline
**this indicates bold text**
_this indicats italic text_

* This is a solid bullet
- This is ab open bullet

[this is the text for a URL](https://iffgit.fz-juelich.de/skelm/containmentfoam/-/issues)
[this is a link to a file in the repository](doc/utilityExamples.md)
-->

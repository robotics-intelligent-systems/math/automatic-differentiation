Contributors
==========
![](doc/files/images/logo_containmentFOAM.png)

_containmentFOAM_ is a multi-species and multi-physics toolbox based on OpenFOAM-9. It is developed on the background of efficiently simulating transport processes inside confined domains e.g. a nuclear reactor containment in the course of a severe accident and may be employed in industrial scale safety assessment. It contains submodels to account for pressurization, heat and mass transfer in buoyant flows, gas radiation heat transport, combustible gas (H<sub>2</sub>/CO)-mixing and mitigation as well as aerosol particle transport.

&larr; [Documentation](README.md)
 
# containmentFOAM Contributors:

* Stephan KELM (Principal Investigator) 2015 -
* Manohar KAMPILI (Turbulence, Aerosols, Maintenance) 2015 -
* Vijaya Kumar GOPALA KRISHNA MOORTHY (Multispecies, Wall Condensation, Turbulence, Aerosols) 2016 -
* Astrid KUHR (Maintenance, Repository) 2016 -
* Xiongguo LIU (Thermal Radiation, REKODIREKT Coupling) 2017 - 
* Markus HORRMANN (REKODIREKT Coupling) 03/2019 - 11/2019
* Kinshiro SAKAMOTO (Wall Condensation) 08/2018 - 2/2019
* Claudia DRUSKA (REKODIREKT development, general validation) 2018 -
* Stephan STRUTH (REKODIREKT development) 2019 -
* Daniel SCHUMACHER (GUI development, Solver Monitor, adaptive time stepping) - 02/2019 -
* Liam M.F. CAMMIADE (Wall Condensation) 2019 -
* Ruiyun JI (Uncertainty Quantification) 2019 -
* Allen GEORGE (Bulk Condensation) 2020 - 
* Lucian RADEMACHER (GUI development) 2020 -

# containmentFOAM Contribution guidelines:

* If you wish to develop/have a new feature, please get in touch with us via [email](mailto:containmentfoam@fz-juelich.de) to discuss its integration. 

* Create an [issue](https://iffgit.fz-juelich.de/containmentfoam_developers/containmentfoam_release9/-/issues) with a consolidated title and sufficient description. Assign the person resposible (if known), while creating the issue itself. 

* To address the issue rised in code development, the assigned person should create a branch and merge request using the option from issue itself. Care should be taken that the *parent branch is develop* (not master), as most recent developments are merged with it. By default, this creates a branch name starting with "issue-number" and merge request with "Draft:" in front.

* Do not push images (plots) into the git. The verification plots, to prove the developed code in the branch works as intended, should be  discussed in the comments section of the relevant merge-request.

* If there is a long standing bug in a specific part of the code, you may start a thread instead of comment in the associated merge-request, which allows a separate discussion.

* When the code is verified and ready to be merged with develop branch, remove the "Draft:" from merge-request and add the assignee reviewer. The reviewer can provide his approval for merging, and it is optional. The assignee should verify compilation, verification discussion in the comments section, and resolve the merge-conflicts,and finally merge with *develop* branch.

* Pushing commits into *develop* and *master* branches is strictly forbidden.

* _containmentFOAM_ is developed under the GNU General Public Licence v3. The act of pushing code to the repository will be understood as an explicit affirmation of the following:
  - The contribution was created in whole or in part by me and I have the right to submit it under the GPL v3 (or the indicated compatible license)
  - My contribution is based upon previous work that, to the best of my knowledge, is covered under the GPL v3 (or the indicated compatible license) and I have the right to submit that work with modifications, under the same open source license, as indicated in the file; Or the contribution was provided directly to me by another person who certified the points above and I have not modified it.
  - My individual contribution is visible in the commit history and globally mentioned in above contributors list. Individual copyright statements should not be added to the source code











<!---
relevant formatting options:

# This indicates a major headline
**this indicates bold text**
_this indicats italic text_

* This is a solid bullet
- This is ab open bullet

[this is the text for a URL](https://iffgit.fz-juelich.de/skelm/containmentfoam/-/issues)
[this is a link to a file in the repository](doc/utilityExamples.md)

-->

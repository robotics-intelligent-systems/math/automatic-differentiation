image: iffregistry.fz-juelich.de/containmentfoam_developers/$CI_PROJECT_NAME:latest

variables:
  REDO: "true"  # true if compilation should be done (#default)!!
  # CI_DO_DOXY: "false"  # true if doxygen should be done

.setup:
  before_script:
    - source /usr/lib64/mpi/gcc/openmpi4/bin/mpivars.sh
    - set +e
    - source /usr/local/cfx/OpenFOAM/OpenFOAM-9/etc/bashrc
    - set -e
    - export PATH=$PATH:./
    - export BASE=$PWD
    - export FOAM_USER_APPBIN=$BASE/mydir/platforms/linux64GccDPInt32Opt/bin
    - export FOAM_USER_LIBBIN=$BASE/mydir/platforms/linux64GccDPInt32Opt/lib
    - export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$FOAM_USER_LIBBIN
    - export PATH=$PATH:$FOAM_USER_APPBIN
    - export PATH=$PATH:$BASE/testCases/tools
    - git config --global --add safe.directory /builds/containmentfoam_developers/$CI_PROJECT_NAME

stages:
  - pre
  - compile
  - tests
  - pages

pre:
  stage: pre
  extends: .setup
  rules:
    - when: always
  script:
    - echo "preparing .."
    - mkdir -p mydir
    - cd mydir
    - rm -rf myresults
    - mkdir -p myresults
    - mkdir -p platforms
    - cd platforms
    - mkdir -p linux64GccDPInt32Opt
    - cd linux64GccDPInt32Opt
    - mkdir -p lib
    - mkdir -p bin
  cache:
    when: always
    paths:
      - mydir
    #key: "$CI_PROJECT_NAME"
    key: "$CI_COMMIT_BRANCH"
  artifacts:
    paths:
    - mydir
#    expire_in: 300 minutes

### build test 

compile:
  stage: compile
  extends: .setup
  rules:
    - when: always
  script:
    - export IBIN=`find $FOAM_USER_APPBIN -type f | wc -l`
    - cd $FOAM_USER_APPBIN
    - pwd
    - ls -la
    - if [ $IBIN == 0 ]; then
      REDO="true";
      else
      echo "reusing older compilation ..";
      fi
    - if [ $REDO == "true" ]; then
      echo "compiling ..";
      cd $BASE;
      export PIPE_NPROC=8;
      sh ./Allwmake.sh > Allwmake.log;
      fi
  cache:
    when: always
    paths:
      - mydir
    #key: "$CI_PROJECT_NAME"
    key: "$CI_COMMIT_BRANCH"
  artifacts:
    paths:
    - Allwmake.log
    - mydir/platforms/linux64GccDPInt32Opt/bin/
    - mydir/platforms/linux64GccDPInt32Opt/lib/
    expire_in: 1 day

### regression tests
###  -wall condensation

wallCond-setcom2d-yPlus1:
  stage: tests
  extends: .setup
  rules:
    - when: always
  variables:
    CASE: yPlus1
  script:
    - cd $BASE
    - cd testCases/wallCondensation/setcom2D
    - /bin/bash ./runCase.sh ${CASE} 

  cache:
    when: always
    paths:
      - mydir
    #key: "$CI_JOB_STAGE"
    #key: "$CI_PROJECT_NAME"
    key: "$CI_COMMIT_BRANCH"
  artifacts:
    paths:
    - testCases/wallCondensation/setcom2D/${CASE}/postProcessing/
    - testCases/wallCondensation/setcom2D/${CASE}/${CASE}.log
    - testCases/wallCondensation/setcom2D/setcom2D-C035_wallFluxes.png
    - mydir
    expire_in: 180 minutes

wallCond-setcom2d-yPlus75:
  stage: tests
  extends:
    - wallCond-setcom2d-yPlus1
  rules:
    - when: always
  variables:
    CASE: yPlus75

wallCond-setcom2d-CHT-yPlus1:
  stage: tests
  extends: .setup
  rules:
    - when: always
  variables:
    CASE: yPlus1
  script:
    - cd $BASE
    - cd testCases/wallCondensation/setcom2D_CHT
    - /bin/bash ./runCase.sh ${CASE} 
  cache:
    when: always
    paths:
      - mydir
    #key: "$CI_JOB_STAGE"
    #key: "$CI_PROJECT_NAME"
    key: "$CI_COMMIT_BRANCH"
  artifacts:
    paths:
    - testCases/wallCondensation/setcom2D_CHT/${CASE}/postProcessing/
    - testCases/wallCondensation/setcom2D_CHT/${CASE}/${CASE}.log
    - testCases/wallCondensation/setcom2D_CHT/setcom2D-C035_wallFluxes.png
    - mydir
    expire_in: 180 minutes
    
###  -bulk condensation
    
bulkCond-MollierMixing:
  stage: tests
  extends: .setup
  rules:
    - when: always
  script:
    - cd $BASE
    - cd testCases/bulkCondensation/MollierMixingNozzle
    - /bin/bash ./Allrun.sh 
   
  cache:
    when: always
    paths:
      - mydir
    #key: "$CI_JOB_STAGE"
    #key: "$CI_PROJECT_NAME"
    key: "$CI_COMMIT_BRANCH"
  artifacts:
    paths:
    - testCases/bulkCondensation/MollierMixingNozzle/postProcessing/
    - testCases/bulkCondensation/MollierMixingNozzle/log
    - testCases/bulkCondensation/MollierMixingNozzle/outletPlot.png
    - testCases/bulkCondensation/MollierMixingNozzle/outletTable.png
    - mydir
    expire_in: 180 minutes    

###  -multi-species  
    
multiSpecies-Cavity4Species:
  stage: tests
  extends: .setup
  rules:
    - when: always
  script:
    - cd $BASE
    - cd testCases/multiSpecies/cavity4Species
    - /bin/bash ./Allrun.sh 
  cache:
    when: always
    paths:
      - mydir
    #key: "$CI_JOB_STAGE"
    #key: "$CI_PROJECT_NAME"
    key: "$CI_COMMIT_BRANCH"
  artifacts:
    paths:
    - testCases/multiSpecies/cavity4Species/postProcessing_cF/
    - testCases/multiSpecies/cavity4Species/cF/log.containmentFluidFoam 
    - testCases/multiSpecies/cavity4Species/results.png 
    - mydir
    expire_in: 180 minutes
    
###  -system models
    
systemModels-burstDisc:
  stage: tests
  extends: .setup
  rules:
    - when: always
  script:
    - cd $BASE
    - cd testCases/systemModels/burstDiscs
    - /bin/bash ./Allrun.sh 
  cache:
    when: always
    paths:
      - mydir
    #key: "$CI_JOB_STAGE"
    #key: "$CI_PROJECT_NAME"
    key: "$CI_COMMIT_BRANCH"
  artifacts:
    paths:
    - testCases/systemModels/burstDiscs/postProcessing/
    - testCases/systemModels/burstDiscs/evalMassFlow.png
    - testCases/systemModels/burstDiscs/evalPressure.png
    - testCases/systemModels/burstDiscs/log
    - mydir
    expire_in: 180 minutes    

systemModels-singlePAR:
  stage: tests
  extends: .setup
  rules:
    - when: always
  script:
    - cd $BASE
    - cd containmentFoamLibs/systemModels/PARs
    - sh makeRD.sh
    - cd $BASE
    - cd testCases/systemModels/PARs/singlePAR
    - /bin/bash ./Allrun.sh 
  cache:
    when: always
    paths:
      - mydir
    #key: "$CI_JOB_STAGE"
    #key: "$CI_PROJECT_NAME"
    key: "$CI_COMMIT_BRANCH"
  artifacts:
    paths:
    - testCases/systemModels/PARs/singlePAR/postProcessing/
    - testCases/systemModels/PARs/singlePAR/comms/
    - testCases/systemModels/PARs/singlePAR/eval_test.png
    - testCases/systemModels/PARs/singlePAR/eval_PARmassBalance.png
    - testCases/systemModels/PARs/singlePAR/log.containmentFluidFoam

    - mydir
    expire_in: 180 minutes        

pages:
  stage: pages
  extends: .setup
  rules:
   - if: '$CI_MERGE_REQUEST_TITLE =~ /^Draft/ && 
          $CI_PIPELINE_SOURCE == "merge_request_event" && 
          $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $CI_DEFAULT_BRANCH'
     when: manual
   - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && 
          $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $CI_DEFAULT_BRANCH || 
          $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == "develop" && $CI_PROJECT_NAME == "containmentfoam9" ||
          $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == "release" && $CI_PROJECT_NAME == "containmentfoam_release9" ' 
  script:
    - cd doc/Doxygen
    - sh ./Allwmake > doxygen.log
    - cd ../..
    - mv doc/Doxygen/html/* public/
  artifacts:
    paths:
    - doc/Doxygen/doxygen.log
    - doc/Doxygen/html
    - public
    expire_in: 300 minutes

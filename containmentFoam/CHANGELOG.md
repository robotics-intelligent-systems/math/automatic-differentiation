Change Log
=======

![](doc/files/images/logo_containmentFOAM.png)

_containmentFOAM_ is a multi-species and multi-physics toolbox based on OpenFOAM-9. It is developed on the background of efficiently simulating transport processes inside confined domains e.g., a nuclear reactor containment in the course of a severe accident. In perspective it may be employed in technical scale safety assessment. It contains submodels to account for pressurization, heat and mass transfer in buoyant flows, gas radiation heat transport, combustible gas (H<sub>2</sub>/CO)-mixing and mitigation as well as aerosol particle transport.

&larr; [Documentation](README.md)

This _changelog_ summarizes the most relevant changes within the _containmentFOAM_ package. The following terminology is used:
* **major** changes add new features or the code was considerably restructured. This may require revision of the validation/application cases,
* **minor** changes do not affect existing validation and setups,
* **bugfixes** do not require changes in existing inputs, but results should be thoroughly checked if affected by the bug.


# Major update of Radiation: Monte Carlo solver can now run on a separate (coarser) mesh, independet of the CFD mesh (March 30th 2023)
* A new option `coarseMesh` was added to the Emission Reciprocity method `ERM` and Shifted Forward `shifted` methods along with the `globalMesh` option. The user can now define a coarser mesh for radiation transport, which allows to decrease the computational cost of the radiation updates significantly. However, as of now, no guideline how to coarsen the CFD mesh exisits. The user should carefully check the mesh-to-mesh projection w.r.t to the radiative wall heat fluxes and the volumetric heat source.
* Documentation [radiation](doc/files/radiation.md) was updated.
* The [testCase](testCases/radiation/SNBCK_H2OCO2) was adapted to showcase the setup of the new model option. 


# Major update of multiSpeciesTransport: Cross-diffusion added to effective binary diffusion model (November 28th 2022)
* A new option was added to the `effectiveDiffivitiy` model (three or more species) which allows to consider the cross-diffusion effect in the diffusive flux. This affects also related sub models like the condensation rate.
* Documentation [governing equations](doc/files/governingEquations.md) and [material and transport properties](doc/files/matProp.md) were updated.
* The [testCase](testCases/multiSpecies/cavity4Species) was adapted and yields now well comparable results to the `Stefan-Maxwell` model, but at considerably lower cost. thus it may improve cases, where diffusive transport is relevant.


# Minor update: integration of Doxygen documentation (November 28th 2022)
* Both code as well as markdown files are documented as a [webpage](https://containmentfoam_developers.iffgit.fz-juelich.de/containmentfoam9/index.html). It is hosted within the repositories (`containmentfoam9`, `containmentfoam_release9`) and can be accessed via the url _https://containmentfoam_developers.iffgit.fz-juelich.de/*repository*/index.html_. 
* Reorganisation of existing markdown based documentation
* Any new markdown files created should be added to the DoxygenLayout.xml to be visible in the related pages tree in html


# Bugfix: MPI problems with Automatic time stepping library  (September 22nd 2022)
* the integration of the automatic time stepping (ATS) function has been refactored. It now does not contain modified versions of the PIMPLE classes anymore, which caused sporadic hang ups of parallel simulation runs. 
* The [ATS functionality](doc/files/timeStepControl.md) is maintained with the new implementation and was tested.


# Bugfix: wall condensation modeling  (September 19th 2022)
* an erroneous if-check in `wallCondensationBase.C` prevented update of turbulent diffusivities by the wall functions. Thus the condensation rates were only calculated using the molecular diffusivitiy and thus too low.
* test and further harmonization of update of[setcom2D - fluidOnly](testCases/wallCondensation/setcom2D) and [setcom2D_CHT](testCases/wallCondensation/setcom2D_CHT) test cases.


# Major update of PAR modeling: REKODIREKT coupling on basis of mass flow rates (September 11th 2022)
* the REKODIREKT code as well as the coupling interface has been refactored to enable a coupling based on mass flow rates (rather than velocities) and mass fractions (rather than volume fractions). This improves mass conservation.
* update of [documentation](doc/files/PAR.md) and [test cases](testCases/systemModels/PARs/singlePAR)


# Minor update: volumetric continuity sources (September 9th 2022)
* [documentation](doc/files/volMassSourceSink.md) and [testCase](testCases/systemModels/volMassSourceSinks) added


# Bugfix: Disable automatic time stepping library due to MPI problem (September 9th 2022)
* for now the automatic time stepping is reverted back to openFOAMs original CFL-based method, since there is an MPI problem to be solved


# Minor update: containmentFOAM version info added to solver log (September 9th 2022)
* the solver log now contains a brief summary about the containmentFoam version, branch, commit hash and build time to aid identfication of potential errors.
* update of [documentation](doc/files/DEBUGGING.md)


# Major update of PAR modeling: REKODIREKT can now evaporate fog (August 26th 2022)
* the REKODIREKT code as well as the coupling interface has been extended to transfer and evaporate the fog which enters the PAR
* update of [documentation](doc/files/PAR.md) and [test cases](testCases/systemModels/PARs/singlePAR)


# Bugfix: Error on burstDisc opening in case of parallel execution (August 17th 2022)
* The burstDisc opens if a certain pressure difference is reached. It is evaluated per partition and thus differences occur in a parallel run if the burstDisc patch is distributed over several mesh partitions. The solution is to sum the pressure difference over all partitions. 
* The burstDisc code was revised accordingly 
* In the case setup, the decomposePar dictionary needs to include the cyclic burstDisc patches within the `preservePatches (xy_side1 xy_side2)` entry, see [test case](testCases/systemModels/burstDiscs/system/decomposeParDict)
* update of [documentation](doc/files/burstDisc.md)


# Major refactoring of the fvMCM RTE solver and SNBCK / LBL models (August 4th 2022)
* there is now a single `fvMCSNBCKH2OCO2` RTE solver, which can handle both steam and CO2 in arbitrary mixtures. Thus, both SNBCK table files need to be added to the case even if one of the species is not present.
* If the local temperature is outside the temperature range of the SNBCK table, it will be clipped for evaluating the absorption coefficient to prevent crashes of the solver.
* the `fvMCMLBL` RTE solver was added to generate line-by-line reference solutions for benchmarking the models
* update of [documentation](doc/files/radiation.md) and [test cases](testCases/radiation)


# Major refactoring of the turbulence model library (August 4th 2022)
* `cfKOmegaSST`:
  - Bugfix in CDKOmega and F1 formulation
  - added buoyancy production/disspation according to cF6
  - refactored source according to OF9
* `buoyantKOmegaSST` renamed to `cfLowReKOmegaSST`. 
  - refactored source according to OF9
  - it is now identical to `cfKOmegaSST` but includes the low-Reynolds damping terms. They can be activated in the momentumProperties dictionary and are only effective in the $`\omega`$ regime of the model, i.e. if a low-Reynolds mesh (y⁺~1) is used.
* all models: the gravity vector is now read from object registry when calculating the source terms and not stored in memory to reduce peak memory consumption
* update of [documentation](doc/files/turbulence.md) and [test cases](testCases/turbulence)


# Bugfix / refactoring adaptiveTimeStepping library (July 15h 2022)
* A memory allocation problem related to the destructor of the `pimpleControl` and `adaptiveTimeStepping` classes was resolved. It caused an error at the very end of a run that may have affected full execution of run-scripts.


# Minor refactoring of the boundary conditions library (July 2nd 2022)
* Since OF6, changes have been introduced to the boundary conditions, wallFunctions etc., which were yet not included in the present version. The following wallFunctions/BCs have been refactored to match the OF9 code:
  - alphatKaderWallFunction
  - alphatNonUnityLewisNumberJayatillekeWallFunction
  - externalCondensingWallHeatFluxTemperature
  - nutUSpaldingFOWallFunction
  - omegaMenterWallFunction
  - nonCondensableMassFraction
  - saturatedSteam
  - turbulentEddyViscosityRatioFrequencyInlet
  - zeroGradientDepositionVelocity
  - burstDisc
* The following tests were successfully conducted to check the modified wallfunctions/BC reproduce, exisiting results
  - wallCondensation/setcom2D - wallFunctionTest
  - wallCondensation/setcom2D_CHT
  - systemModels/burstDiscs
  - turbulence/channel2D 
  - turbulence/buoyantSquareCavity_Ra1e9
  - radiation/fvMCM

# Major refactoring of the PAR modeling (June, 10th 2022)
* `externalCoupled`: Revised external coupling for PARs to consider arbitrary interface orientations and cross-sections with new surfaceNormal boundary condition. For modeling PAR operation in a containment, one has to consider different PAR sizes and orientations. This was limited in previous version due to hard coded velocity vector. In this release, the PAR inlet/outlet velocity magnitude, obtained from REKODIREKT is transferred to a surface normal vector using the new `externalCoupleSurfaceNormalFixedValue` boundary condition. Furthermore, as the PAR inlet/outlet boundaries can have different sizes the interface area is written by the `externalCoupled` functionObject into the patchAreas file to enable scaling of the mapped velocities between REKODIREKT and OpenFOAM to ensure mass conservation when using the velocity as measure for the interface flux. 
* added automatic region detection to `controlRekoDirekt.py`. This now does not require changes in `rd_input.json` when switching from fluid only to multiRegion cases
* the PAR [test cases](testCases/systemModels/PARs) were updated, the new treatment was tested and proved to be robust and conservative.
* update of [documentation](doc/files/turbulence.md) 


# Minor refactoring of turbulent transport definitions (Mar, 15th 2022)
* `thermophysicalTranport`: The turbulent Prandtl and Schmidt numbers were orginally defined in the `turbulenceProperties` dict in cF6. Within OF9 this definition was moved into `thermophysicalTranport`. Even though _containmentFOAM_ uses its own `multiSpeciesMassTransport` library, this definition was moved to avoid ambiguities and resulting deviations in the results. The library now throws an error if the `thermophysicalTransport` dictionary is not available and reads the values from it. Definitions in `momentumTransport`dictionary are not considered anymore
* update of the test cases


# Major update: integration of adaptiveTimeStepping (February 16th 2022)
* the timeSteps can now be determined by different criteria such as the number of PIMPLE loops
* timeSteps can be repeated if convergence is not reached or a CFL number is exceeded
* `systemEvents` have been introduced to enable apriori adaption of the timeStep if e.g. a burstDics opens or other dynamic events are approaching
* [documentation](doc/files/timeStepControl.md) added
* [burstDisc](testCases/systemModels/burstDiscs) testCase updated


# Major update: integration of Monte Carlo radiation models (January 17th 2022)
* the fvMCM ration transport equation solver was added
* different algorithms: `normal`, `shifted` and `ERM` can be tested, while the latter two are most efficient
* parallelization of photon tracking on the `globalMesh` added
* [documentation](doc/files/radiation.md) added
* [testCase](testCases/radiation) added


# Major update: bulkCondensation models added (September 29th 2021)
* the `multiSpeciesMassTranport` library was refactored and all wallCondensation related functions were transferred into the [condensationModels](containmentFoamLibs/condensationModels) library
* `bulkCondensation`
   - [documentation](doc/files/bulkCondensation.md) added
   - [test cases](testCases/bulkCondensation) added


# Major update: code ported from OF6 to OF9
* functionObjects (January 11th 2022)
* containmentFOAM CHT solver (October 20th 2021)
* PAR models (October 14th 2021)
* System models (September 6th 2021)
* boundary conditions (August 25th 2021)
* multiSpeciesMassTranport models (August 3rd 2021)


<!---

what to put here:
# Tag name (Date of merge)
**Release highlights**
Explain the changes in free words and link to documentation for general info

**Features**
* _revisedXYZ_: brief statement and link to commit hash ([#233243](https://iffgit.fz-juelich.de/containmentfoam_developers/containmentfoam9/-/commit/8a88160192e45f2bdff206dd962bdee1198bfcae))

relevant formatting options:

# This indicates a major headline
**this indicates bold text**
_this indicats italic text_

* This is a solid bullet
- This is ab open bullet

[this is the text for a URL](https://iffgit.fz-juelich.de/skelm/containmentfoam/-/issues)
[this is a link to a file in the repository](doc/utilityExamples.md)

-->

